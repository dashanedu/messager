<?php


namespace Dashan\Messager\Classes;


class PutLogsResponse extends Response
{
    public function __construct($headers) {
        parent::__construct( $headers );
    }
}