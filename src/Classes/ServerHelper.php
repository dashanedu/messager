<?php

namespace Dashan\Messager\Classes;

class ServerHelper
{
    protected static $request_id;
    /**
     * 获取请求协议
     */
    public static function getRequestScheme()
    {
        return isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : '';
    }

    /**
     * 获取请求方法
     */
    public static function getRequestMethod()
    {
        return isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
    }

    /**
     * 获取请求客户端信息
     */
    public static function getClientAgent()
    {
        return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }

    /**
     * 获取请求客户端ip
     */
    public static function getClientAddr()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip     =   $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * 获取请求域名
     */
    public static function getHttpHost()
    {
        return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
    }

    /**
     * 自动获取服务器IP
     */
    public static function getServerAddr()
    {
        return isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
    }

    /**
     * 自动获取服务器域名
     */
    public static function getRequestUri()
    {
        return isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    }

    /**
     * 自动获取服务器端口号
     */
    public static function getServerPort()
    {
        return isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : '';
    }

    /**
     * 获取请求编号
     */
    public static function getRequestId()
    {
        if (!self::$request_id) {
            self::$request_id = self::generateRequestId();
        }
        return self::$request_id;
    }

    /**
     * 生成请求编号
     * @return mixed|string
     */
    public static function generateRequestId()
    {
        if (isset($_POST['request_id'])) {
            return $_POST['request_id'];
        }
        if (isset($_GET['request_id'])) {
            return $_GET['request_id'];
        }

        $numbers = '123456789';
        list($micro_str, $second_str) = explode(' ', microtime());
        $request_id = date('YmdHis', $second_str)
            . substr($micro_str, strpos($micro_str, '.')+1)
            . substr(str_shuffle($numbers), 0, 4);
        return $request_id;
    }

    /**
     * 自动获取用户信息
     * @return array
     */
    public static function getStudentInfo()
    {
        $student_id = 0;
        $stu_phone = '';
        if (function_exists('session')) {
            $student_info = session('wap.student');
            if (isset($student_info) && !empty($student_info)) {
                $student_id = isset($student_info['id']) ? intval($student_info['id']) : $student_id;
                $stu_phone = isset($student_info['u_phone']) ? intval($student_info['u_phone']) : $stu_phone;
            }
        } elseif (isset($_POST['student_id'])) {
            $student_id = intval($_POST['student_id']);
        }

        return [
            'student_id' => $student_id,
            'stu_phone' => $stu_phone
        ];
    }

    /**
     * 自动获取管理员信息
     * @return array
     */
    public static function getUserInfo()
    {
        $user_id = 0;
        $u_phone = '';
        if (function_exists('session')) {
            $student_info = session('wap.student');
            if (!empty($student_info)) {
                $user_id = isset($student_info['id']) ? intval($student_info['id']) : $user_id;
                $u_phone = isset($student_info['u_phone']) ? intval($student_info['u_phone']) : $u_phone;
            }
        } elseif (isset($_POST['teacher_id'])) {
            $user_id = intval($_POST['teacher_id']);
        }

        return [
            'user_id' => $user_id,
            'u_phone' => $u_phone
        ];
    }
}
