<?php


namespace Dashan\Messager\Classes;

use Dashan\Messager\Exceptions\AliyunLogException;

class SimpleLogger {

    /**
     * internal cache for log messages
     * @var array
     */
    private $logItems = [];

    /**
     * max size of cached messages
     * @var int
     */
    private $maxCacheLog;

    /**
     * log topic field
     * @var
     */
    private $topic;

    /**
     * max time before logger post the cached messages
     * @var int
     */
    private $maxWaitTime;

    /**
     * previous time for posting log messages
     * @var int
     */
    private $previousLogTime;

    /**
     * max storage size for cached messages
     * @var int
     */
    private $maxCacheBytes;

    /**
     * messages storage size for cached messages
     * @var int
     */
    private $cacheBytes;

    /**
     * log client which was wrappered by this logger
     * @var log
     */
    private $client;

    /**
     * log project name
     * @var the
     */
    private $project;

    /**
     * logstore name
     * @var the
     */
    private $logstore;

    /**
     * AliyunLog_LogBatch constructor.
     * @param $client log client
     * @param $project the corresponding project
     * @param $logstore the logstore
     * @param $topic
     * @param null $maxCacheLog max log items limitation, by default it's 100
     * @param null $maxWaitTime max thread waiting time, bydefault it's 5 seconds
     */
    public function __construct($client, $project, $logstore, $topic, $maxCacheLog = null, $maxWaitTime = null, $maxCacheBytes = null)
    {
        if(NULL === $maxCacheLog || !is_integer($maxCacheLog)){
            $this->maxCacheLog = 100;
        }else{
            $this->maxCacheLog = $maxCacheLog;
        }

        if(NULL === $maxCacheBytes || !is_integer($maxCacheBytes)){
            $this->maxCacheBytes = 256 * 1024;
        }else{
            $this->maxCacheBytes = $maxCacheBytes;
        }

        if(NULL === $maxWaitTime || !is_integer($maxWaitTime)){
            $this->maxWaitTime = 5;
        }else{
            $this->maxWaitTime = $maxWaitTime;
        }
        if($client == null || $project == null || $logstore == null){
            throw new \Exception('the input parameter is invalid! create SimpleLogger failed!');
        }
        $this->client = $client;
        $this->project = $project;
        $this->logstore = $logstore;
        $this->topic = $topic;
        $this->previousLogTime = time();
        $this->cacheBytes = 0;
    }

    /**
     * add logItem to cached array, and post the cached messages when cache reach the limitation
     * @param $cur_time
     * @param $logItem
     */
    private function logItem($cur_time, $logItem){
        array_push($this->logItems, $logItem);
        if ($cur_time - $this->previousLogTime >= $this->maxWaitTime || sizeof($this->logItems) >= $this->maxCacheLog
            || $this->cacheBytes >= $this->maxCacheBytes)
        {
            $this->logBatch($this->logItems, $this->topic);
            $this->logItems = [];
            $this->previousLogTime = time();
            $this->cacheBytes = 0;
        }
    }

    /**
     * log single string message
     * @param LogLevel $logLevel
     * @param $logMessage
     * @param $collect_no
     * @throws Exception
     */
    private function logSingleMessage($logMessage, $collect_no, $position){
        if(is_array($logMessage) || !is_string($logMessage)){
            $logMessage = json_encode($logMessage, JSON_UNESCAPED_UNICODE);
        }

        $cur_time = time();
        $contents = [
            'time' => date('Y-m-d H:i:s', $cur_time),
            'timestamp' => $cur_time,
            'client_agent' => ServerHelper::getClientAgent(),
            'client_addr' => ServerHelper::getClientAddr(),
            'server_addr' => ServerHelper::getServerAddr(),
            'server_port' => ServerHelper::getServerPort(),
            'runtime' => $position,
            'collect_no' => $collect_no,
            'body' => $logMessage
        ];

        $this->cacheBytes += strlen($logMessage) + 32;
        $logItem = new LogItem();
        $logItem->setTime($cur_time);
        $logItem->setContents($contents);
        $this->logItem($cur_time, $logItem);
    }

    /**
     * log array message
     * @param LogLevel $logLevel
     * @param $logMessage
     * @param $collect_no
     * @throws \Exception
     */
    private function logArrayMessage($logMessage, $collect_no, $position){
        if(!is_array($logMessage)){
            throw new \Exception('input message is not array, please use logSingleMessage!');
        }

        $cur_time = time();
        $contents = [
            'time' => date('Y-m-d H:i:s', $cur_time),
            'timestamp' => $cur_time,
            'client_agent' => ServerHelper::getClientAgent(),
            'client_addr' => ServerHelper::getClientAddr(),
            'server_addr' => ServerHelper::getServerAddr(),
            'server_port' => ServerHelper::getServerPort(),
            'runtime' => $position,
            'collect_no' => $collect_no,
        ];

        foreach ($logMessage as $key => $value)
        {
            if (is_array($value) || !is_string($value)) {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
            $contents[$key] = $value;
            $this->cacheBytes += strlen($key) + strlen($value);
        }
        $this->cacheBytes += 32;
        $logItem = new LogItem();
        $logItem->setTime($cur_time);
        $logItem->setContents($contents);
        $this->logItem($cur_time, $logItem);
    }

    /**
     * submit string log message with info level
     * @param $logMessage
     */
    public function send($collect_no, $logMessage){
        $traces = debug_backtrace();
        $last_trace = $traces[1];
        $position = $last_trace['file'].':'.$last_trace['line'];
        $this->logSingleMessage($logMessage, $collect_no, $position);
    }

    /**
     * submit array log message with info level
     * @param $logMessage
     */
    public function sendArray($collect_no, $logMessage){
        $traces = debug_backtrace();
        $last_trace = $traces[1];
        $position = $last_trace['file'].':'.$last_trace['line'];
        $this->logArrayMessage($logMessage, $collect_no, $position);
    }

    /**
     * get current machine IP
     * @return string
     */
    private function getLocalIp(){
        $local_ip = getHostByName(php_uname('n'));
        if(strlen($local_ip) == 0){
            $local_ip = getHostByName(getHostName());
        }
        return $local_ip;
    }

    /**
     * submit log messages in bulk
     * @param $logItems
     * @param $topic
     */
    private function logBatch($logItems, $topic){
        $ip = $this->getLocalIp();
        $request = new PutLogsRequest($this->project, $this->logstore,
            $topic, $ip, $logItems);
        $error_exception = NULL;
        for($i = 0 ;  $i < 3 ; $i++)
        {
            try{
                $response = $this->client->putLogs($request);
                return;
            } catch (AliyunLogException $ex) {
                $error_exception = $ex;
            } catch (\Exception $ex) {
                var_dump($ex);
                $error_exception = $ex;
            }
        }
        if ($error_exception != NULL)
        {
            var_dump($error_exception);
        }
    }

    /**
     * manually flush all cached log to log server
     */
    public function logFlush(){
        if(sizeof($this->logItems) > 0){
            $this->logBatch($this->logItems, $this->topic);
            $this->logItems = [];
            $this->previousLogTime= time();
            $this->cacheBytes = 0;
        }
    }

    function __destruct() {
        if(sizeof($this->logItems) > 0){
            $this->logBatch($this->logItems, $this->topic);
        }
    }
}