<?php

namespace Dashan\Messager;

use Dashan\Messager\Classes\AliyunLogClient;
use Dashan\Messager\Classes\LoggerFactory;

class Messager
{
    protected $logger;
    protected $endpoint;
    protected $accessKeyId;
    protected $accessKey;
    protected $logProject;
    protected $logStore;
    protected $logTopic;

    public function __construct()
    {
        $logConfig = config('logger.aliyun-sls');
        $this->endpoint = $logConfig['endpoint'];
        $this->accessKeyId = $logConfig['accessKeyId'];
        $this->accessKey = $logConfig['accessKey'];
        $this->logProject = $logConfig['logProject'];
        $client = new AliyunLogClient($this->endpoint, $this->accessKeyId, $this->accessKey);
        $this->logger = LoggerFactory::getLogger($client, $this->logProject, $this->logStore, $this->logTopic);
    }

    public function send($collect_no, $message)
    {
        $this->logger->send($collect_no, $message);
    }

    public function sendArray($collect_no, $messages)
    {
        $this->logger->sendArray($collect_no, $messages);
    }
}